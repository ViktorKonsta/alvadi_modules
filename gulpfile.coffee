# =================================\
# Define
# =================================/

$ = require './gulp/auto-require'
cfg = require './gulp/config'
fs = require 'fs'
path = require 'path'
subprojectRex = /[:][a-z]+/g

params = null
min = no

for one in process.argv
	if one is '--min' then min = on
	if one.match subprojectRex then params = one

note =
	green: (msg) ->
		$.util.log $.util.colors.green msg

note.green 'parameters: ' + params
note.green 'minification: ' + min

rucksackBundle =
	$.rucksackCss
		autoprefixer: on
		fallbacks: on

# =================================\
# Server-SetUp
# =================================/

do $.browserSync.create

server =
	setup: (base) ->
		$.browserSync.init
			server:
				baseDir: base
				routes:
					"/css": "dist/#{resource}/css/"
					"/js": "dist/#{resource}/js/"
					"/svg-sprite": "dist/#{resource}/svg-sprite/"
	reload: ->
		do $.browserSync.stream

# =================================\
# Global tasks
# =================================/

if params.match subprojectRex

	[resource, project] = params.split ':'

	$.gulp.task params, ->
		$.runSequence ["styles", "scripts", "svg", "templates"], "server", "watch"

$.gulp.task "styles", ->
	$.gulp.src "#{cfg.src}/#{resource}/styles/*.styl"
		.pipe do $.plumber
		.pipe $.stylus
			'include css': on
			use: [ $.poststylus [
				rucksackBundle
				require 'postcss-flexibility'
				require 'postcss-color-mix'
			] ]
		.pipe do $.csso
		.pipe $.gulp.dest "#{cfg.dist}/#{resource}/css/"
		.pipe do server.reload

$.gulp.task "scripts", ->
	$.gulp.src "#{cfg.src}/#{resource}/scripts/#{resource}.ext.coffee"
		.pipe do $.plumber
		.pipe do $.coffeeify
		# .pipe do $.uglify
		.pipe $.gulp.dest "#{cfg.dist}/#{resource}/js/"
		.pipe do server.reload

$.gulp.task "svg", ->
	$.gulp.src "#{cfg.src}/#{resource}/svg/*.svg"
		.pipe $.svgstore
			inlineSvg: yes
		.pipe $.rename "svg-sprite.svg"
		.pipe $.gulp.dest "#{cfg.dist}/#{resource}/svg-sprite/"
		.pipe do server.reload

$.gulp.task "templates", ->

	pages = []

	pagesList = fs.readdirSync "#{cfg.src}/#{resource}/templates/pages/#{project}/"
	
	for one in pagesList
		if one is 'index.jade' then continue
		pages.push path.basename one, '.jade'

	$.gulp.src "#{cfg.src}/#{resource}/templates/pages/#{project}/*.jade"
		.pipe do $.plumber
		.pipe $.jade
			pretty: yes
			locals: pages: pages
		.pipe $.newer "#{cfg.dist}/#{resource}/html/#{project}/"
		.pipe $.gulp.dest "#{cfg.dist}/#{resource}/html/#{project}/"
		.pipe do server.reload

$.gulp.task "server", ->
	server.setup "#{cfg.dist}/#{resource}/html/#{project}/"

$.gulp.task "watch", ->

	$.gulp.watch [
		"#{cfg.src}/#{resource}/styles/**/*.styl"
		"#{cfg.src}/common/styles/**/*.styl"],
		["styles"]

	$.gulp.watch "#{cfg.src}/#{resource}/scripts/**/*.coffee", ["scripts"]

	$.gulp.watch "#{cfg.src}/#{resource}/svg/*.svg", ["svg"]

	$.gulp.watch [
		"#{cfg.src}/#{resource}/templates/pages/#{project}/**/*.jade"
		"#{cfg.src}/#{resource}/templates/layouts/*.jade"
		"#{cfg.src}/common/templates/**/*.jade"],
		["templates"]