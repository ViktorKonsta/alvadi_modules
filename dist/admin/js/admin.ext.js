(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
module.exports = window.alvadi = {
  popup: require('./popup'),
  loading: require('./loading'),
  Utils: require('./utils')
};

},{"./loading":2,"./popup":3,"./utils":4}],2:[function(require,module,exports){
var Loading;

Loading = (function() {
  function Loading() {
    this.loadingScope = $('.loading');
  }

  Loading.prototype.init = function() {
    return this.loadingScope.addClass('show');
  };

  Loading.prototype.end = function() {
    return this.loadingScope.removeClass('show');
  };

  return Loading;

})();

module.exports = function() {
  return new Loading;
};

},{}],3:[function(require,module,exports){
var Popup, Utils;

Utils = require('../utils');

Popup = (function() {
  function Popup(options) {
    var close, closeBtn, init, selector;
    if (!Utils.there(options.scope)) {
      return;
    }
    this.scope = options.scope, selector = options.selector, close = options.close, init = options.init;
    this.body = $('body');
    if (init) {
      init(options);
    }
    if (selector) {
      selector.on('click', (function(_this) {
        return function(e) {
          return _this.open();
        };
      })(this));
    } else {
      this.open();
    }
    closeBtn = this.scope.find(close);
    closeBtn.on('click', (function(_this) {
      return function(e) {
        return _this.close();
      };
    })(this));
    this.scope.on('click', function(e) {
      if (e.target === $(this)[0]) {
        return closeBtn.trigger('click');
      }
    });
  }

  Popup.prototype.open = function() {
    this.scope.addClass('show-popup');
    return this.body.addClass('no-scroll');
  };

  Popup.prototype.close = function() {
    this.scope.removeClass('show-popup');
    return this.body.removeClass('no-scroll');
  };

  return Popup;

})();

module.exports = function(options) {
  return new Popup(options);
};

},{"../utils":4}],4:[function(require,module,exports){
module.exports = {
  there: require('./there'),
  toggleCheck: require('./toggle-check')
};

},{"./there":5,"./toggle-check":6}],5:[function(require,module,exports){

/**
	* This function check if element that we
	* need in this page. If not then return 'false'.
	* It can be useful to avoid useless execution.
	@param {jQuery} element - element to find
 */
module.exports = function(element) {
  if ($('body').find(element)[0]) {
    return true;
  }
  return false;
};

},{}],6:[function(require,module,exports){

/**
	* This function check the checkbox if it's
	* not checked yet. And disable if it's checked.
	* Return input
	@param {jQuery} Object - input to check
 */
module.exports = function(input) {
  if (input.is(':checked')) {
    input.removeAttr('checked');
    return input;
  }
  input.attr('checked', '');
  return input;
};

},{}],7:[function(require,module,exports){
/**
 * lodash 4.0.1 (Custom Build) <https://lodash.com/>
 * Build: `lodash modularize exports="npm" -o ./`
 * Copyright 2012-2016 The Dojo Foundation <http://dojofoundation.org/>
 * Based on Underscore.js 1.8.3 <http://underscorejs.org/LICENSE>
 * Copyright 2009-2016 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
 * Available under MIT license <https://lodash.com/license>
 */

/** Used as the `TypeError` message for "Functions" methods. */
var FUNC_ERROR_TEXT = 'Expected a function';

/** Used as references for various `Number` constants. */
var NAN = 0 / 0;

/** `Object#toString` result references. */
var funcTag = '[object Function]',
    genTag = '[object GeneratorFunction]';

/** Used to match leading and trailing whitespace. */
var reTrim = /^\s+|\s+$/g;

/** Used to detect bad signed hexadecimal string values. */
var reIsBadHex = /^[-+]0x[0-9a-f]+$/i;

/** Used to detect binary string values. */
var reIsBinary = /^0b[01]+$/i;

/** Used to detect octal string values. */
var reIsOctal = /^0o[0-7]+$/i;

/** Built-in method references without a dependency on `root`. */
var freeParseInt = parseInt;

/** Used for built-in method references. */
var objectProto = Object.prototype;

/**
 * Used to resolve the [`toStringTag`](http://ecma-international.org/ecma-262/6.0/#sec-object.prototype.tostring)
 * of values.
 */
var objectToString = objectProto.toString;

/* Built-in method references for those with the same name as other `lodash` methods. */
var nativeMax = Math.max;

/**
 * Gets the timestamp of the number of milliseconds that have elapsed since
 * the Unix epoch (1 January 1970 00:00:00 UTC).
 *
 * @static
 * @memberOf _
 * @type Function
 * @category Date
 * @returns {number} Returns the timestamp.
 * @example
 *
 * _.defer(function(stamp) {
 *   console.log(_.now() - stamp);
 * }, _.now());
 * // => logs the number of milliseconds it took for the deferred function to be invoked
 */
var now = Date.now;

/**
 * Creates a debounced function that delays invoking `func` until after `wait`
 * milliseconds have elapsed since the last time the debounced function was
 * invoked. The debounced function comes with a `cancel` method to cancel
 * delayed `func` invocations and a `flush` method to immediately invoke them.
 * Provide an options object to indicate whether `func` should be invoked on
 * the leading and/or trailing edge of the `wait` timeout. The `func` is invoked
 * with the last arguments provided to the debounced function. Subsequent calls
 * to the debounced function return the result of the last `func` invocation.
 *
 * **Note:** If `leading` and `trailing` options are `true`, `func` is invoked
 * on the trailing edge of the timeout only if the the debounced function is
 * invoked more than once during the `wait` timeout.
 *
 * See [David Corbacho's article](http://drupalmotion.com/article/debounce-and-throttle-visual-explanation)
 * for details over the differences between `_.debounce` and `_.throttle`.
 *
 * @static
 * @memberOf _
 * @category Function
 * @param {Function} func The function to debounce.
 * @param {number} [wait=0] The number of milliseconds to delay.
 * @param {Object} [options] The options object.
 * @param {boolean} [options.leading=false] Specify invoking on the leading
 *  edge of the timeout.
 * @param {number} [options.maxWait] The maximum time `func` is allowed to be
 *  delayed before it's invoked.
 * @param {boolean} [options.trailing=true] Specify invoking on the trailing
 *  edge of the timeout.
 * @returns {Function} Returns the new debounced function.
 * @example
 *
 * // Avoid costly calculations while the window size is in flux.
 * jQuery(window).on('resize', _.debounce(calculateLayout, 150));
 *
 * // Invoke `sendMail` when clicked, debouncing subsequent calls.
 * jQuery(element).on('click', _.debounce(sendMail, 300, {
 *   'leading': true,
 *   'trailing': false
 * }));
 *
 * // Ensure `batchLog` is invoked once after 1 second of debounced calls.
 * var debounced = _.debounce(batchLog, 250, { 'maxWait': 1000 });
 * var source = new EventSource('/stream');
 * jQuery(source).on('message', debounced);
 *
 * // Cancel the trailing debounced invocation.
 * jQuery(window).on('popstate', debounced.cancel);
 */
function debounce(func, wait, options) {
  var args,
      maxTimeoutId,
      result,
      stamp,
      thisArg,
      timeoutId,
      trailingCall,
      lastCalled = 0,
      leading = false,
      maxWait = false,
      trailing = true;

  if (typeof func != 'function') {
    throw new TypeError(FUNC_ERROR_TEXT);
  }
  wait = toNumber(wait) || 0;
  if (isObject(options)) {
    leading = !!options.leading;
    maxWait = 'maxWait' in options && nativeMax(toNumber(options.maxWait) || 0, wait);
    trailing = 'trailing' in options ? !!options.trailing : trailing;
  }

  function cancel() {
    if (timeoutId) {
      clearTimeout(timeoutId);
    }
    if (maxTimeoutId) {
      clearTimeout(maxTimeoutId);
    }
    lastCalled = 0;
    args = maxTimeoutId = thisArg = timeoutId = trailingCall = undefined;
  }

  function complete(isCalled, id) {
    if (id) {
      clearTimeout(id);
    }
    maxTimeoutId = timeoutId = trailingCall = undefined;
    if (isCalled) {
      lastCalled = now();
      result = func.apply(thisArg, args);
      if (!timeoutId && !maxTimeoutId) {
        args = thisArg = undefined;
      }
    }
  }

  function delayed() {
    var remaining = wait - (now() - stamp);
    if (remaining <= 0 || remaining > wait) {
      complete(trailingCall, maxTimeoutId);
    } else {
      timeoutId = setTimeout(delayed, remaining);
    }
  }

  function flush() {
    if ((timeoutId && trailingCall) || (maxTimeoutId && trailing)) {
      result = func.apply(thisArg, args);
    }
    cancel();
    return result;
  }

  function maxDelayed() {
    complete(trailing, timeoutId);
  }

  function debounced() {
    args = arguments;
    stamp = now();
    thisArg = this;
    trailingCall = trailing && (timeoutId || !leading);

    if (maxWait === false) {
      var leadingCall = leading && !timeoutId;
    } else {
      if (!maxTimeoutId && !leading) {
        lastCalled = stamp;
      }
      var remaining = maxWait - (stamp - lastCalled),
          isCalled = remaining <= 0 || remaining > maxWait;

      if (isCalled) {
        if (maxTimeoutId) {
          maxTimeoutId = clearTimeout(maxTimeoutId);
        }
        lastCalled = stamp;
        result = func.apply(thisArg, args);
      }
      else if (!maxTimeoutId) {
        maxTimeoutId = setTimeout(maxDelayed, remaining);
      }
    }
    if (isCalled && timeoutId) {
      timeoutId = clearTimeout(timeoutId);
    }
    else if (!timeoutId && wait !== maxWait) {
      timeoutId = setTimeout(delayed, wait);
    }
    if (leadingCall) {
      isCalled = true;
      result = func.apply(thisArg, args);
    }
    if (isCalled && !timeoutId && !maxTimeoutId) {
      args = thisArg = undefined;
    }
    return result;
  }
  debounced.cancel = cancel;
  debounced.flush = flush;
  return debounced;
}

/**
 * Checks if `value` is classified as a `Function` object.
 *
 * @static
 * @memberOf _
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is correctly classified, else `false`.
 * @example
 *
 * _.isFunction(_);
 * // => true
 *
 * _.isFunction(/abc/);
 * // => false
 */
function isFunction(value) {
  // The use of `Object#toString` avoids issues with the `typeof` operator
  // in Safari 8 which returns 'object' for typed array constructors, and
  // PhantomJS 1.9 which returns 'function' for `NodeList` instances.
  var tag = isObject(value) ? objectToString.call(value) : '';
  return tag == funcTag || tag == genTag;
}

/**
 * Checks if `value` is the [language type](https://es5.github.io/#x8) of `Object`.
 * (e.g. arrays, functions, objects, regexes, `new Number(0)`, and `new String('')`)
 *
 * @static
 * @memberOf _
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an object, else `false`.
 * @example
 *
 * _.isObject({});
 * // => true
 *
 * _.isObject([1, 2, 3]);
 * // => true
 *
 * _.isObject(_.noop);
 * // => true
 *
 * _.isObject(null);
 * // => false
 */
function isObject(value) {
  var type = typeof value;
  return !!value && (type == 'object' || type == 'function');
}

/**
 * Converts `value` to a number.
 *
 * @static
 * @memberOf _
 * @category Lang
 * @param {*} value The value to process.
 * @returns {number} Returns the number.
 * @example
 *
 * _.toNumber(3);
 * // => 3
 *
 * _.toNumber(Number.MIN_VALUE);
 * // => 5e-324
 *
 * _.toNumber(Infinity);
 * // => Infinity
 *
 * _.toNumber('3');
 * // => 3
 */
function toNumber(value) {
  if (isObject(value)) {
    var other = isFunction(value.valueOf) ? value.valueOf() : value;
    value = isObject(other) ? (other + '') : other;
  }
  if (typeof value != 'string') {
    return value === 0 ? value : +value;
  }
  value = value.replace(reTrim, '');
  var isBinary = reIsBinary.test(value);
  return (isBinary || reIsOctal.test(value))
    ? freeParseInt(value.slice(2), isBinary ? 2 : 8)
    : (reIsBadHex.test(value) ? NAN : +value);
}

module.exports = debounce;

},{}],8:[function(require,module,exports){
var alvadi, dataToggleHandler, datepickerHandler, debounce, extBtnDisableBehavior, navigationAndLeftbar, setupOrder, tablePerRowSelectHandler, there;

alvadi = require('alvadi-components');

dataToggleHandler = require('../../common/scripts/data-toggle-handler');

datepickerHandler = require('../../common/scripts/datepicker-handler');

tablePerRowSelectHandler = require('../../common/scripts/table-per-row-select');

debounce = require('lodash.debounce');

there = alvadi.Utils.there;

window.v = {};

extBtnDisableBehavior = function() {
  return $('.ext-btn.disabled').off('click');
};

v.navSearchInput = function(callback) {
  var inputs, resetBtn;
  resetBtn = $('[data-reset]');
  resetBtn.hide();
  inputs = $(resetBtn.data('reset'));
  resetBtn.on('click', function() {
    inputs.val('');
    return resetBtn.hide();
  });
  return inputs.on('keyup', debounce(function() {
    var length, self;
    self = $(this);
    length = self.val().length;
    if (length > 0) {
      resetBtn.show();
    } else {
      resetBtn.hide();
    }
    if (callback && self.val() !== '') {
      callback(self.val());
    }
  }, 400));
};

v.enableBtn = function(btn) {
  btn.removeClass('disabled');
  extBtnDisableBehavior();
};

v.disableBtn = function(btn) {
  btn.addClass('disabled');
  extBtnDisableBehavior();
};

setupOrder = function() {
  var setupOrderBtn;
  setupOrderBtn = $('.setup-order');
  if (!there(setupOrderBtn)) {
    return;
  }

  /*
  		NB! This is only DEMO script
  		It's only for demonstrations
  		of how it should work!
  		Don't use it on production!
   */
  setupOrderBtn.on('click', function() {
    var closestNote, self, selfTable;
    self = $(this);
    closestNote = self.parent().parent().parent().prev();
    selfTable = self.parent().parent().parent();
    closestNote.removeClass('note-hidden');
    return selfTable.hide();
  });
  $('.close-note').on('click', function() {
    return $(this).parent().parent().hide();
  });
};

v.popupTransportComplaint = function(callback) {
  var loading, popupTransportComplaint, transportComplaintPopupTrigger;
  popupTransportComplaint = $('.popup-transport-complaint');
  if (!there(popupTransportComplaint)) {
    return;
  }
  loading = alvadi.loading();
  transportComplaintPopupTrigger = $('.transport-complaint-popup-trigger');
  transportComplaintPopupTrigger.on('click', function() {
    var popup1;
    popup1 = alvadi.popup({
      scope: popupTransportComplaint,
      close: $('.close-popup-trigger')
    });
    return popupTransportComplaint.find('.popup-trigger-send').on('click', function() {
      var $one, data, i, len, one, ref;
      if (callback) {
        data = [];
        ref = popupTransportComplaint.find('.popup__content').find('input, select, textarea');
        for (i = 0, len = ref.length; i < len; i++) {
          one = ref[i];
          $one = $(one);
          if ($one.attr('type').match(/checkbox|radio/g)) {
            if ($one.is(':checked')) {
              data.push($one.attr('id'));
              continue;
            }
          }
          data.push($one.val());
        }
        callback(data);
      }
      popup1.close();
      return alvadi.popup({
        scope: $('.popup-transport-complaint-success'),
        close: $('.close-popup-trigger'),
        init: function() {
          transportComplaintPopupTrigger.addClass('disabled');
          return extBtnDisableBehavior();
        }
      });
    });
  });
};

v.popupReturnSupplier = function(returnSupplierCallback, returnSupplierCreditNoteCallback) {
  var popup1, popupReturnSupplier, productCreditNoteBtn, returnSupplierPopupTrigger, submitPopup;
  popupReturnSupplier = $('.popup-return-supplier');
  returnSupplierPopupTrigger = $('.return-supplier-popup-trigger');
  if (!there(popupReturnSupplier)) {
    return;
  }
  submitPopup = $('.submit-popup');
  productCreditNoteBtn = $('.product-credit-note');
  popup1 = alvadi.popup({
    scope: popupReturnSupplier,
    close: $('.close-popup-trigger'),
    selector: returnSupplierPopupTrigger
  });
  submitPopup.on('click', function() {
    popup1.close();
    if (returnSupplierCallback) {
      returnSupplierCallback();
    }
    return alvadi.popup({
      scope: $('.popup-return-supplier-success'),
      close: $('.close-popup-trigger'),
      init: function() {
        v.enableBtn(productCreditNoteBtn);
        v.disableBtn(returnSupplierPopupTrigger);
        extBtnDisableBehavior();
        return productCreditNoteBtn.on('click', function() {
          if (returnSupplierCreditNoteCallback) {
            returnSupplierCreditNoteCallback();
          }
          return alvadi.popup({
            scope: $('.popup-return-supplier-credit-note'),
            close: $('.close-popup-trigger'),
            init: function() {
              v.disableBtn(productCreditNoteBtn);
              return extBtnDisableBehavior();
            }
          });
        });
      }
    });
  });
};

v.popupProductCreditNote = function(callback) {
  var popupCreditNote, popupNoReasonTrigger;
  popupCreditNote = $('.popup-credit-note');
  if (!there(popupCreditNote)) {
    return;
  }
  popupNoReasonTrigger = $('.popup-credit-note-trigger');
  popupNoReasonTrigger.on('click', function() {
    alvadi.popup({
      scope: popupCreditNote,
      close: $('.close-popup-trigger')
    });
    v.disableBtn(popupNoReasonTrigger);
    if (callback) {
      return callback();
    }
  });
};

$(function() {
  dataToggleHandler();
  datepickerHandler();
  tablePerRowSelectHandler();
  extBtnDisableBehavior();
  setupOrder();
  v.popupTransportComplaint(function(data) {
    return console.log(data);
  });
  v.popupReturnSupplier(function() {
    return console.log('return to supplier');
  }, function() {
    return console.log('credit note done!');
  });
  v.navSearchInput(function(value) {
    return console.log(value);
  });
  v.popupProductCreditNote(function() {
    return console.log('ajax');
  });
});

navigationAndLeftbar = function() {
  var btnLeftbarToggler, btnMoreIndicators, isMenuActivable, leftbar, moreIndicators, profile, profileBox, profilePhoto;
  btnLeftbarToggler = $('#leftbar-toggle');
  btnMoreIndicators = $('#btn-more-indicators');
  moreIndicators = $('#more-indicators');
  leftbar = $('.xe-leftbar');
  isMenuActivable = true;
  profileBox = $('.nav-profile');
  profilePhoto = $('.nav-profile-photo');
  profile = profileBox.add(profilePhoto);
  profile.hover(function() {
    profilePhoto.addClass('active');
    return profileBox.removeClass('hidden');
  }, function() {
    profilePhoto.removeClass('active');
    return profileBox.addClass('hidden');
  });
  $(document).on('keydown', function(e) {
    var m;
    m = 77;
    $('input, textarea').not('input[type="checkbox"], input[type="radio"]').on('focus', function() {
      isMenuActivable = false;
      return $(this).on('blur', function() {
        return isMenuActivable = true;
      });
    });
    if (e.keyCode === m && isMenuActivable) {
      return btnLeftbarToggler.trigger('click');
    }
  });
  btnLeftbarToggler.on('click', function(e) {
    e.preventDefault();
    leftbar.toggleClass('hidden');
    btnLeftbarToggler.toggleClass('active');
    $('body').toggleClass('no-scroll');
    return $('.overlay').toggleClass('hidden');
  });
  return btnMoreIndicators.on('click', function(e) {
    e.preventDefault();
    btnMoreIndicators.toggleClass('active');
    return moreIndicators.toggleClass('hidden');
  });
};

$(function() {
  return navigationAndLeftbar();
});

},{"../../common/scripts/data-toggle-handler":9,"../../common/scripts/datepicker-handler":10,"../../common/scripts/table-per-row-select":11,"alvadi-components":1,"lodash.debounce":7}],9:[function(require,module,exports){
var alvadi, there;

alvadi = require('alvadi-components');

there = alvadi.Utils.there;

module.exports = function() {
  var allInputs, dataToggleElement, isInput;
  dataToggleElement = $('[data-toggle]');
  if (!there(dataToggleElement)) {
    return;
  }
  if (dataToggleElement[0].tagName === 'INPUT') {
    isInput = true;
    allInputs = $('input[type="radio"], input[type="checkbox"]').not(dataToggleElement);
    allInputs.on('click', function() {
      var self;
      self = $(this);
      dataToggleElement.trigger('click');
      return self.attr('checked', '');
    });
  }
  dataToggleElement.on('click', function() {
    var self, toggleId;
    self = $(this);
    toggleId = self.data('toggle');
    if (isInput) {
      if (self.is(':checked')) {
        $(toggleId).removeClass('hidden');
      } else {
        $(toggleId).addClass('hidden');
      }
      return;
    }
    return $(toggleId).toggleClass('hidden');
  });
};

},{"alvadi-components":1}],10:[function(require,module,exports){
var alvadi, there;

alvadi = require('alvadi-components');

there = alvadi.Utils.there;

module.exports = function() {
  var datepickerEl;
  datepickerEl = $('.datepicker');
  if (!there(datepickerEl)) {
    return;
  }
  datepickerEl.datepicker({
    changeMonth: true,
    changeYear: true,
    showButtonPanel: true,
    dateFormat: "dd.mm.yy",
    duration: "fast",
    firstDay: 1,
    showAnim: "fade",
    showOptions: {
      direction: "up"
    }
  });
};

},{"alvadi-components":1}],11:[function(require,module,exports){
var alvadi, there, toggleCheck;

alvadi = require('alvadi-components');

there = alvadi.Utils.there;

toggleCheck = alvadi.Utils.toggleCheck;

module.exports = function() {
  var checkboxInput, table, tableWithCheckBoxSelectable, tbody, tbodyTr, thead;
  tableWithCheckBoxSelectable = $('table.checkbox-selectable');
  if (!there(tableWithCheckBoxSelectable)) {
    return;
  }
  table = $('table');
  thead = tableWithCheckBoxSelectable.find('thead');
  tbody = tableWithCheckBoxSelectable.find('tbody');
  tbodyTr = tbody.find('tr');
  checkboxInput = $('input[type="checkbox"]');
  thead.on('click', function(e) {
    var i, j, len, len1, one, self, tbodyCollection, theadCheckbox;
    self = $(this);
    theadCheckbox = self.find(checkboxInput);
    tbodyCollection = self.parent().find('tbody').find(checkboxInput);
    if (!theadCheckbox.is(':checked')) {
      for (i = 0, len = tbodyCollection.length; i < len; i++) {
        one = tbodyCollection[i];
        $(one).attr('checked', '');
      }
      return theadCheckbox.attr('checked', '');
    } else {
      for (j = 0, len1 = tbodyCollection.length; j < len1; j++) {
        one = tbodyCollection[j];
        $(one).removeAttr('checked');
      }
      return theadCheckbox.removeAttr('checked');
    }
  });
  tbodyTr.on('click', function(e) {
    var self, selfCheckBox, theadCheckbox;
    self = $(this);
    selfCheckBox = self.find(checkboxInput);
    theadCheckbox = self.parent().parent().find('thead').find(checkboxInput);
    if (!theadCheckbox.is(':checked')) {
      if (e.target === selfCheckBox[0]) {
        return;
      }
      return toggleCheck(selfCheckBox);
    } else {
      theadCheckbox.removeAttr('checked');
      if (e.target === selfCheckBox[0]) {
        return;
      }
      return toggleCheck(selfCheckBox);
    }
  });
};

},{"alvadi-components":1}]},{},[8]);
