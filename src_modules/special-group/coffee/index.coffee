$ ->

	there = alvadi.Utils.there
	toggleCheck = alvadi.Utils.toggleCheck

	deleteGroupPopup = ->
		
		popup = $ '.popup-delete-group'

		if not there popup then return

		delGroupBtn = $('.del-group-popup-trigger')

		delGroupBtn.on 'click', ->
			alvadi.popup
				scope: popup
				close: popup.find('.close-popup-trigger')

		###
			Чтобы отследить действия по кнопке "Удалить", следует
			задать элементу ID. Затем, в этой функции прописать все
			нужные действия. 
		###

		return

	tablePerRowSelect = ->

		tableWithCheckBoxSelectable = $ 'table.checkbox-selectable'
		if not there tableWithCheckBoxSelectable then return
		thead = tableWithCheckBoxSelectable.find 'thead'
		tbody = tableWithCheckBoxSelectable.find 'tbody'
		tbodyTr = tbody.find 'tr'
		checkboxInput = $ 'input[type="checkbox"]'

		thead.on 'click', (e) ->
			self = $ @
			theadCheckbox = self.find checkboxInput
			tbodyCollection = self.parent().find('tbody').find checkboxInput

			if not theadCheckbox.is ':checked'
				$(one).attr('checked', '') for one in tbodyCollection
				theadCheckbox.attr 'checked', ''
			else
				$(one).removeAttr('checked') for one in tbodyCollection
				theadCheckbox.removeAttr 'checked'

		tbodyTr.on 'click', (e) ->

			self = $ @
			selfCheckBox = self.find checkboxInput
			theadCheckbox = self.parent().parent().find('thead').find checkboxInput

			if not theadCheckbox.is ':checked'
				if e.target is selfCheckBox[0] then return
				toggleCheck selfCheckBox
			else
				theadCheckbox.removeAttr 'checked'
				if e.target is selfCheckBox[0] then return
				toggleCheck selfCheckBox

		return

	dateInput = ->
		dateInput = $('.date-input')
		if not there dateInput then return
		dateInput.datepicker
			changeMonth: true
			changeYear: true
			showButtonPanel: true
			dateFormat: "dd.mm.yy"
			duration: "fast"
			firstDay: 1
			showAnim: "fade"
			showOptions: direction: "up"
		return

	tablePerRowSelect()
	deleteGroupPopup()
	dateInput()

	return