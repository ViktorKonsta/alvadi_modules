
alvadi = require 'alvadi-components'
there = alvadi.Utils.there

module.exports = ->

	datepickerEl = $ '.datepicker'

	if not there datepickerEl then return

	datepickerEl.datepicker
		changeMonth: true
		changeYear: true
		showButtonPanel: true
		dateFormat: "dd.mm.yy"
		duration: "fast"
		firstDay: 1
		showAnim: "fade"
		showOptions: direction: "up"
		
	return