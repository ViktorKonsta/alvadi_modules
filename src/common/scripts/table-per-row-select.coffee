
alvadi = require 'alvadi-components'
there = alvadi.Utils.there
toggleCheck = alvadi.Utils.toggleCheck

module.exports = ->

	tableWithCheckBoxSelectable = $ 'table.checkbox-selectable'
	if not there tableWithCheckBoxSelectable then return

	table = $ 'table'
	thead = tableWithCheckBoxSelectable.find 'thead'
	tbody = tableWithCheckBoxSelectable.find 'tbody'
	tbodyTr = tbody.find 'tr'
	checkboxInput = $ 'input[type="checkbox"]'

	thead.on 'click', (e) ->
		self = $ @
		theadCheckbox = self.find checkboxInput
		tbodyCollection = self.parent().find('tbody').find checkboxInput

		if not theadCheckbox.is ':checked'
			$(one).attr('checked', '') for one in tbodyCollection
			theadCheckbox.attr 'checked', ''
		else
			$(one).removeAttr('checked') for one in tbodyCollection
			theadCheckbox.removeAttr 'checked'

	tbodyTr.on 'click', (e) ->
		self = $ @
		selfCheckBox = self.find checkboxInput
		theadCheckbox = self.parent().parent().find('thead').find checkboxInput

		if not theadCheckbox.is ':checked'
			if e.target is selfCheckBox[0] then return
			toggleCheck selfCheckBox
		else
			theadCheckbox.removeAttr 'checked'
			if e.target is selfCheckBox[0] then return
			toggleCheck selfCheckBox

	return
