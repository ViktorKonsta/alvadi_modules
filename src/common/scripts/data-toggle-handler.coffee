
alvadi = require 'alvadi-components'
there = alvadi.Utils.there

module.exports = ->
	dataToggleElement = $ '[data-toggle]'

	if not there dataToggleElement then return

	if dataToggleElement[0].tagName is 'INPUT'
		isInput = yes
		allInputs = $('input[type="radio"], input[type="checkbox"]').not dataToggleElement
		allInputs.on 'click', ->
			self = $ @
			dataToggleElement.trigger 'click'
			self.attr 'checked', ''

	dataToggleElement.on 'click', ->
		self = $ @
		toggleId = self.data 'toggle'

		if isInput
			if self.is ':checked'
				$(toggleId).removeClass 'hidden'
			else
				$(toggleId).addClass 'hidden'
			return

		$(toggleId).toggleClass 'hidden'

	return