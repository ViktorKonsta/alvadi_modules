
alvadi = require 'alvadi-components'

dataToggleHandler = require '../../common/scripts/data-toggle-handler'
datepickerHandler = require '../../common/scripts/datepicker-handler'
tablePerRowSelectHandler = require '../../common/scripts/table-per-row-select'

debounce = require 'lodash.debounce'

there = alvadi.Utils.there
window.v = {}

extBtnDisableBehavior = -> $('.ext-btn.disabled').off 'click'

v.navSearchInput = (callback) ->
	resetBtn = $ '[data-reset]'
	resetBtn.hide()
	inputs = $ resetBtn.data 'reset'
	resetBtn.on 'click', ->
		inputs.val ''
		resetBtn.hide()
	inputs.on 'keyup', debounce ->
		self = $ @
		length = self.val().length
		if length > 0
			resetBtn.show()
		else
			resetBtn.hide()
		if callback and self.val() isnt ''
			callback self.val()
		return
	, 400

v.enableBtn = (btn) ->
	btn.removeClass 'disabled'
	extBtnDisableBehavior()
	return
v.disableBtn = (btn) ->
	btn.addClass 'disabled'
	extBtnDisableBehavior()
	return

setupOrder = ->
	setupOrderBtn = $('.setup-order')
	if not there setupOrderBtn then return

	###
		NB! This is only DEMO script
		It's only for demonstrations
		of how it should work!
		Don't use it on production!
	###

	setupOrderBtn.on 'click', ->
		self = $ @
		closestNote = self.parent().parent().parent().prev()
		selfTable = self.parent().parent().parent()

		closestNote.removeClass 'note-hidden'
		selfTable.hide()

	$('.close-note').on 'click', ->
		$(@).parent().parent().hide()
	return

v.popupTransportComplaint = (callback) ->
	popupTransportComplaint = $('.popup-transport-complaint')
	if not there popupTransportComplaint then return

	loading = alvadi.loading()
	transportComplaintPopupTrigger = $('.transport-complaint-popup-trigger')

	transportComplaintPopupTrigger.on 'click', ->
		popup1 = alvadi.popup
			scope: popupTransportComplaint
			close: $('.close-popup-trigger')

		popupTransportComplaint.find('.popup-trigger-send').on 'click', ->
			if callback
				data = []
				for one in popupTransportComplaint.find('.popup__content').find('input, select, textarea')
					$one = $ one
					if $one.attr('type').match(/checkbox|radio/g)
						if $one.is ':checked'
							data.push $one.attr 'id'
							continue
					data.push $one.val()
				callback data
			popup1.close()
			alvadi.popup
				scope: $('.popup-transport-complaint-success')
				close: $('.close-popup-trigger')
				init: ->
					transportComplaintPopupTrigger.addClass 'disabled'
					extBtnDisableBehavior()
	return

v.popupReturnSupplier = (returnSupplierCallback, returnSupplierCreditNoteCallback) ->
	popupReturnSupplier = $('.popup-return-supplier')
	returnSupplierPopupTrigger = $('.return-supplier-popup-trigger')

	if not there popupReturnSupplier then return

	submitPopup = $ '.submit-popup'
	productCreditNoteBtn = $ '.product-credit-note'

	popup1 = alvadi.popup
		scope: popupReturnSupplier
		close: $('.close-popup-trigger')
		selector: returnSupplierPopupTrigger

	submitPopup.on 'click', ->
		popup1.close()
		if returnSupplierCallback then do returnSupplierCallback
		alvadi.popup
			scope: $('.popup-return-supplier-success')
			close: $('.close-popup-trigger')
			init: ->
				v.enableBtn productCreditNoteBtn
				v.disableBtn returnSupplierPopupTrigger
				extBtnDisableBehavior()
				productCreditNoteBtn.on 'click', ->
					if returnSupplierCreditNoteCallback then do returnSupplierCreditNoteCallback
					alvadi.popup
						scope: $('.popup-return-supplier-credit-note')
						close: $('.close-popup-trigger')
						init: ->
							v.disableBtn productCreditNoteBtn
							extBtnDisableBehavior()
	return

v.popupProductCreditNote = (callback) ->
	popupCreditNote = $('.popup-credit-note')
	if not there popupCreditNote then return

	popupNoReasonTrigger = $('.popup-credit-note-trigger')

	popupNoReasonTrigger.on 'click', ->
		alvadi.popup
			scope: popupCreditNote
			close: $('.close-popup-trigger')
		v.disableBtn popupNoReasonTrigger
		if callback then do callback
	return

$ ->
	dataToggleHandler()
	datepickerHandler()
	tablePerRowSelectHandler()
	extBtnDisableBehavior()

	setupOrder()

	v.popupTransportComplaint (data) ->
		console.log data

	v.popupReturnSupplier ->
		console.log 'return to supplier'
	, ->
		console.log 'credit note done!'

	v.navSearchInput (value) ->
		console.log value

	v.popupProductCreditNote ->
		console.log 'ajax'

	return

navigationAndLeftbar = ->
	btnLeftbarToggler = $ '#leftbar-toggle'
	btnMoreIndicators = $ '#btn-more-indicators'
	moreIndicators = $ '#more-indicators'
	leftbar = $ '.xe-leftbar'
	isMenuActivable = yes
	profileBox = $ '.nav-profile'
	profilePhoto = $ '.nav-profile-photo'
	profile = profileBox.add profilePhoto

	profile.hover ->
		profilePhoto.addClass 'active'
		profileBox.removeClass 'hidden'
	, ->
		profilePhoto.removeClass 'active'
		profileBox.addClass 'hidden'

	$(document).on 'keydown', (e) ->
		m = 77

		$('input, textarea').not('input[type="checkbox"], input[type="radio"]').on 'focus', ->
			isMenuActivable = no
			$(@).on 'blur', ->
				isMenuActivable = yes

		if e.keyCode is m and isMenuActivable
			btnLeftbarToggler.trigger 'click'

	btnLeftbarToggler.on 'click', (e) ->
		e.preventDefault()
		leftbar.toggleClass 'hidden'
		btnLeftbarToggler.toggleClass 'active'
		$('body').toggleClass 'no-scroll'
		$('.overlay').toggleClass 'hidden'

	btnMoreIndicators.on 'click', (e) ->
		e.preventDefault()
		btnMoreIndicators.toggleClass 'active'
		moreIndicators.toggleClass 'hidden'

$ ->
	navigationAndLeftbar()