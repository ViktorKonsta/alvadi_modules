alvadi = require 'alvadi-components'

window.v = {}

dataToggleHandler = do require '../../common/scripts/data-toggle-handler'
datepickerHandler = do require '../../common/scripts/datepicker-handler'
tablePerRowSelect = do require '../../common/scripts/table-per-row-select'
Modernizr = require '../../common/scripts/modernizr'
flexibility = require 'flexibility'

do require 'svg4everybody'

there = alvadi.Utils.there

v.subscribeFooter = (callback) ->
	footer = $ '.main-footer'
	if not there footer then return

	subscribeBtn = $ '#footer-subscribe-btn'
	subscribeInput = $ '#footer-subscribe'
	footerSubPart = $ '#footer-sub'
	footerSuccessPart = $ '#footer-success'
	anotherSubBtn = $ '#another-sub'
	footerHeight = footer.height()
	pageContainer = $('.page_container')
	pageContainer.css 'padding-bottom': footerHeight + 'px'

	emailCheck = /[a-z0-9-_.]+[@][a-z]+[.][a-z]+/g
	yourSubsEmail = $ '#your-subs-email'

	tooltip = $ '#footer-error-tooltip'

	subscribeBtn.on 'click', (e) ->
		e.preventDefault()

		if subscribeInput.val().match emailCheck
			subscribeInputVal = subscribeInput.val()
			subscribeInput.val ''
			subscribeBtn.blur()
			footerSubPart.addClass 'hidden'
			footerSuccessPart.removeClass 'hidden'
			yourSubsEmail.text subscribeInputVal
			if callback then callback subscribeInputVal
			return

		tooltip.addClass 'show'
		subscribeInput.focus()

	anotherSubBtn.on 'click', ->
		footerSubPart.removeClass 'hidden'
		footerSuccessPart.addClass 'hidden'

	subscribeInput.on 'keydown', (e) ->
		tooltip.removeClass 'show'

	return

carFilterRemoveActive = ->

	carFilter = $ '#car-filter'

	if not there carFilter then return

	linksInFilter = carFilter.find 'a'
	activeLink = carFilter.find '.active'

	linksInFilter.on 'click', ->
		activeLink.removeClass 'active'

deleteProductFromReturn = ->
	popup = $ '#popup-delete-product'
	if not there popup then return

	console.log 'yay'

	$('.data-table-btn-delete').on 'click', (e) ->
		e.preventDefault()
		alvadi.popup
			scope: popup
			close: $ '.close-popup-trigger'

carFilterRemoveActive()
deleteProductFromReturn()